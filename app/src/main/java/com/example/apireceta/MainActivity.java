package com.example.apireceta;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private TextView tvBusqueda;
    private TextView tvIngredientes;
    private TextView tvNutricion;
    private Button btBuscar;
    private TextView tvReceta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.tvBusqueda = (TextView) findViewById(R.id.TextB);
        this.btBuscar = (Button) findViewById(R.id.BtBuscar);
        this.tvIngredientes = (TextView) findViewById(R.id.tvIngredientes);
        this.tvNutricion = (TextView) findViewById(R.id.tvNutricion);
        this.tvReceta = (TextView) findViewById(R.id.tvReceta);


        this.btBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String b = tvBusqueda.getText().toString().trim();

                if(b.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Ingrese el nombre de la receta", Toast.LENGTH_SHORT).show();
                }else {
                    String url = "https://api.edamam.com/search?q=" + b + "&app_id=17a73c66&app_key=9eb0f9ecf1858c5d8d44bfd85e789db3";
                    StringRequest solicitud = new StringRequest(
                            Request.Method.GET,
                            url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // Tenemos respuesta desde el servidor
                                    try {
                                        JSONObject respuestaJSON = new JSONObject(response);


                                        JSONArray hitsJSON = respuestaJSON.getJSONArray("hits");
                                        JSONObject primer = hitsJSON.getJSONObject(0);
                                        JSONObject recipesJSON = primer.getJSONObject("recipe");
                                        JSONArray in = recipesJSON.getJSONArray("ingredientLines");
                                        String titulo = recipesJSON.getString("label");
                                        String ingre = "";

                                        for (int i = 0; i < in.length(); i++) {
                                            ingre = ingre + in.getString(i) + "\n";
                                        }


                                        JSONObject nutJSON = recipesJSON.getJSONObject("totalNutrients");
                                        JSONObject energiaJSON = nutJSON.getJSONObject("ENERC_KCAL");
                                        String energia_q = energiaJSON.getString("quantity");
                                        String energia_u = energiaJSON.getString("unit");

                                        JSONObject fatJSON = nutJSON.getJSONObject("FAT");
                                        String fat_q = fatJSON.getString("quantity");
                                        String fat_u = fatJSON.getString("unit");

                                        JSONObject fasatJSON = nutJSON.getJSONObject("FASAT");
                                        String fasat_q = fasatJSON.getString("quantity");
                                        String fasat_u = fasatJSON.getString("unit");


                                        String todo = "(Energía) \n" + "-Cantidad: " + energia_q + " Unidad: " + energia_u + " "+ "\n" +
                                                "(Grasa) \n" + "-Cantidad: " + fat_q + " Unidad: " + fat_u + " " + "\n" +
                                                "(Grasas saturadas) \n" + "-Cantidad: " + fasat_q + " Unidad: " + fasat_u + " " + "\n";
                                        tvBusqueda.setText("");
                                        tvReceta.setText(titulo);
                                        tvIngredientes.setText(ingre);

                                        tvNutricion.setText(todo);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // Algo fallo
                                }
                            }
                    );

                    RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                    listaEspera.add(solicitud);
                }
            }
        });
    }
}
